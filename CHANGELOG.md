# Changelog

## 0.3.5 (2024-03-04)

### Mejoras

- Actualización de dependencias a versiones más recientes
- Corrección del atributo hidden en SdTpaginate para usar vinculación booleana
- Mejora de accesibilidad en el selector de filas por página
- Actualización de la configuración de Husky para pre-commit hooks

### Correcciones

- Resuelto problema de seguridad en esbuild
- Mejora en los tests para evitar fallos

## 0.3.4 (versión anterior)

- Versión publicada anteriormente
