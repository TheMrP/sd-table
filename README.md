# \<sd-table>

A simple and lightweight component for convert your data in table.
You can use your JSON array directly or through the fetch dedicated worker.

## Installation

```bash
npm i sd-table
```

## Options

For more information of options.fetch see [use fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch) at MDN

```js
{
  enablePagination: boolean,

  enableSort: boolean,

  noSort: [],

  onlySort: [],

  bordered: boolean,

  striped: boolean,

  fullwidth: boolean,

  stickyHeader: boolean,

  paginationItems: 3, // to fix (n * 2 + 1) n = 3 => 7 items

  fetch: {
    uri: string,
    options: object
  }
}
```

## Example

Using a JSON array

```js
import { html, LitElement } from 'lit';
import 'sd-table/sd-table.js';

export class App extends LitElement {
  render() {
    const options = {
      enablePagination: false,
      enableSort: false,
      striped: true
    };

    const data = [
      {columnOne: 'text 1'},
      {columnTwo: 1},
      {columnThree: html`<button>click me</button>`}
    ];

    return html`
    <sd-table id="sd-table-data-example"
              .data=${data}
              .header=${[
                'Custom header',
                'Custom header 2',
                'Custom header 3']}
              .options=${options}>
    </sd-table>`;
  }
}
```

Fetching a JSON array

```js
import { html, LitElement } from 'lit';
import 'sd-table/sd-table.js';

export class App extends LitElement {
  render() {
    const options = {
      striped: true,
      fullwidth: true,
      fetch: {
        uri: "/api/orders/filter-order-product",
        options: {
          headers: {
            'Content-Type': 'application/json',
          },
          method: 'POST'
        }
      }

    };

    return html`
    <sd-table id="sd-table-fetch-example"
              .options=${options}
              @change=${}>
    </sd-table>`;
  }
}
```

## Styles

```stylus
:root {
  #arrows
  --sd-thead-arrow-color: #FFF;
  --sd-paginate-arrow-color: #000;

  #thead
  --sd-thead-text-align: center
  --sd-thead-font-style:;
  --sd-thead-font-weight: bold;
  --sd-thead-font-size: 1rem;
  --sd-thead-vertical-align: middle;
  --sd-thead-border:;
  --sd-thead-border-color:;
  --sd-thead-border-top:;
  --sd-thead-border-right:;
  --sd-thead-border-bottom:;
  --sd-thead-border-left:;
  --sd-thead-justify-content: center;
  --sd-thead-color: #000;
  --sd-thead-background: #FFF;
  --sd-thead-padding: 0.3rem;

  #tbody
  --sd-tbody-font-size: 0.9rem;
  --sd-tbody-text-align: justify;
  --sd-tbody-vertical-align: middle;
  --sd-tbody-color: #5c5c5c;

  #pagination
  --sd-paginate-color: #000;
  --sd-paginate-item-border-color: #000;

  #options
  --sd-bordered-border: solid 1px #ccc;
  --sd-striped-odd-color: #efefef;
  --sd-striped-even-color: #ffffff;
}
```

It ain't much, but it's honest work
