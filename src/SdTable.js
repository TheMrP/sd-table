import { html, LitElement } from 'lit';
// import { repeat } from 'lit/directives/repeat.js';
import { first, next, back, last, up, down, upDown } from './Arrows.js';
import { fetchWorker } from './fetch_worker.js';
import { sortWorker } from './sort_worker.js';
import { sdTableStyle, paginateStyle } from './css.js';

export class SdTable extends LitElement {
  static get styles() {
    return [sdTableStyle, paginateStyle];
  }

  static get defaultOptions() {
    return {
      enableSort: true,
      enablePagination: true,
      stickyHeader: true,
    };
  }

  static get properties() {
    return {
      data: { type: Array },
      header: { type: Array },
      options: { type: Object },
      page: { type: Number },
      rowsPerPage: { type: Number },
      sort: { type: Object },
    };
  }

  constructor() {
    super();

    this.data = [];
    this.header = [];
    this.options = SdTable.defaultOptions;
    this.page = 1;
    this.numberPages = 1;
    this.rowsPerPage = 15;
    this.sort = {};
    this.rowsCount = 0;
    this.colsCount = 0;
  }

  /*
  shouldUpdate(changedProperties) {

    console.log(changedProperties)
    // Only update element if prop1 changed.
    return changedProperties.has('page');
  }
  */

  updated(_changedProperties) {
    super.updated(_changedProperties);
    this.dispatchEvent(
      new CustomEvent('change', {
        detail: {
          pagination: {
            page: this.page,
            numberPages: this.numberPages,
            rowsPerPage: this.rowsPerPage,
          },
          rows: this.rowsCount,
          cols: this.colsCount,
          sort: this.sort,
        },
      })
    );
  }

  setData(data) {
    this.data = data.data;
  }

  connectedCallback() {
    super.connectedCallback();

    if (window.Worker) {
      this.sortWorker = new Worker(sortWorker);
      this.fetchWorker = new Worker(fetchWorker);
      this.sortWorker.onmessage = e => {
        this.setData(e);
      };
      this.fetchWorker.onmessage = e => {
        this.setData(e);
      };
    }
    // eslint-disable-next-line no-console
    else console.log("Your browser doesn't support web workers.");
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.sortWorker.terminate();
    this.fetchWorker.terminate();
  }

  firstUpdated() {
    super.firstUpdated();
    this.options = { ...SdTable.defaultOptions, ...this.options };
    if (this.options.fetch) this.fetchData();
  }

  order() {
    this.sortWorker.postMessage(this.data, this.order);
  }

  fetchData() {
    this.fetchWorker.postMessage(this.options.fetch);
  }

  handleChange(e) {
    const rowsPerPage = e.target.value || 15;
    this.handleClickPage(1);
    this.rowsPerPage = rowsPerPage;
  }

  handleClickPage(p = 1, numberPages = this.numberPages) {
    let page = 1;
    if (p !== 0) page = p >= 1 && p <= numberPages ? p : numberPages;
    this.page = page;
  }

  handleClickSort(sort) {
    const sorter = { ...this.sort, ...sort };
    const keys = Object.keys(sorter);
    for (let i = 0; i < keys.length; i += 1)
      if (sorter[keys[i]] === null) delete sorter[keys[i]];
    const aux = Object.keys(sorter);
    try {
      if (aux.length > 0)
        this.sortWorker.postMessage({ data: this.data, sort: sorter });
    } catch (error) {
      this.sortData(sorter);
    }

    this.sort = sorter;
  }

  // eslint-disable-next-line class-methods-use-this
  getValue(value) {
    const typeOf = typeof value;
    if (typeOf === 'string') {
      const cleanValue = value.trim().replace(',', '');
      const auxValue = cleanValue * 1 || 0;
      return auxValue !== 0 && cleanValue !== '0' ? auxValue : value;
    }
    if (typeOf === 'number') return value;
    if (typeOf === 'function') return value.value || value.toString();

    return '';
  }

  sortData(sort) {
    const rows = this.data;
    const sortKeys = Object.keys(sort);

    Promise.all(
      rows.sort((a, b) => {
        let aCount = 0;
        let bCount = 0;

        for (let l = 0; l < sortKeys.length; l += 1) {
          const sortKey = sortKeys[l];
          const order = sort[sortKey];
          const sortDirection = order === 'asc' ? -1 : 1;

          const aValue = this.getValue(a[sortKey]);
          const bValue = this.getValue(b[sortKey]);

          if (typeof aValue === 'string' || typeof bValue === 'string') {
            const compare = aValue.toString().localeCompare(bValue.toString());
            if (compare > 0) aCount += sortDirection;

            if (compare < 0) bCount += sortDirection;
          } else {
            if (aValue > bValue) aCount += sortDirection;

            if (aValue < bValue) bCount += sortDirection;
          }
        }

        if (aCount > bCount) return 1;

        if (aCount < bCount) return -1;

        return 0;
      })
    ).then(data => this.setData({ data }));
  }

  thead(data, sort = this.sort) {
    const enableSort = !!this.options.enableSort;
    let result = html` <td>No data for head</td>`;
    const noSort =
      this.options.noSort && this.options.noSort.length > 0
        ? this.options.noSort
        : [];
    const onlySort =
      this.options.onlySort && this.options.onlySort.length > 0
        ? this.options.onlySort
        : data;
    const header =
      Array.isArray(this.header) && this.header.length > 0
        ? this.header
        : false;

    if (data.length > 0)
      result = data.map((col, i) => {
        if (enableSort && !noSort.includes(col) && onlySort.includes(col)) {
          const orderColumn = sort[col] || null;
          let icon = upDown;
          let nextOrder = 'desc';
          if (orderColumn !== null) {
            icon = orderColumn === 'asc' ? up : down;
            nextOrder = orderColumn === 'asc' ? null : 'asc';
          }

          return html` <th>
            <button
              @click=${e => {
                e.preventDefault();
                this.handleClickSort({ [col]: nextOrder });
              }}
            >
              <span>${icon}</span>${header ? header[i] : col}
            </button>
          </th>`;
        }

        return html` <th>${col}</th>`;
      });

    return result;
  }

  // thead() { }

  // eslint-disable-next-line class-methods-use-this
  tbody(rows) {
    /*
    const { key } = this.options;
    key
      ? repeat(
          rows,
          row => row[key],
          row => {
            const columns = Object.values(row);
            const result = columns.map(td => html`<td>${td}</td>`);
            return html`<tr>
              ${result}
            </tr>`;
          }
        )
      */

    return rows.map(row => {
      const columns = Object.values(row);
      const result = columns.map(td => html`<td>${td}</td>`);
      return html`<tr>
        ${result}
      </tr>`;
    });
  }

  pagination(numberPages, rowsPerPage, page, rowsCount) {
    const pages = [];
    const pageItems =
      this.options.paginationItems >= 1 ? this.options.paginationItems : 3;
    const enablePagination = !!this.options.enablePagination;
    if (numberPages > 1) {
      pages.push(
        html` <button
            @click=${e => {
              e.preventDefault();
              this.handleClickPage(1, numberPages);
            }}
          >
            ${first}
          </button>
          <button
            @click=${e => {
              e.preventDefault();
              this.handleClickPage(page - 1, numberPages);
            }}
          >
            ${back}
          </button>`
      );

      for (let i = 1; i <= numberPages; i += 1) {
        const active = i === page ? 'active' : '';
        const minLimit = pageItems * 2 + 1;
        const show =
          (page <= pageItems && i <= minLimit) ||
          (i >= page - pageItems && i <= page + pageItems) ||
          (page >= numberPages - pageItems && i > numberPages - minLimit);

        if (show)
          pages.push(
            html` <button
              @click=${e => {
                e.preventDefault();
                this.handleClickPage(i, numberPages);
              }}
              class=${active}
            >
              ${i}
            </button>`
          );
      }
      pages.push(
        html` <button
            @click=${e => {
              e.preventDefault();
              this.handleClickPage(page + 1, numberPages);
            }}
          >
            ${next}
          </button>
          <button
            @click=${e => {
              e.preventDefault();
              this.handleClickPage(numberPages, numberPages);
            }}
          >
            ${last}
          </button>`
      );
    }

    const options = [];
    for (let i = 15; i <= 105; i += 15)
      options.push(
        rowsPerPage === i
          ? html` <option value=${i} selected>${i}</option>`
          : html` <option value=${i}>${i}</option>`
      );

    // eslint-disable-next-line lit-a11y/no-invalid-change-handler
    const select = html`&nbsp;<select @change=${this.handleChange}>
        ${options}</select
      >&nbsp;`;

    return enablePagination
      ? html`<div class="sd-pagination">
          <div>
            Showing ${select} of ${rowsCount} rows per page. Page ${page} of
            ${numberPages}
          </div>
          <div>${pages}</div>
        </div>`
      : null;
  }

  render() {
    const data = this.data || [];
    const { options, sort } = this;
    const fullWidth = options.fullWidth ? 'full-width' : null;
    const bordered = options.bordered ? 'bordered' : null;
    const striped = options.striped ? 'striped' : null;
    const stickyHeader = options.stickyHeader ? 'sticky-header' : null;

    let thead = options.fetch ? html`<td>Loading</td>` : html`<td>Void</td>`;
    let tbody = options.fetch
      ? html`<tr>
          <td><progress></progress></td>
        </tr>`
      : html`<tr>
          <td>data not found :(</td>
        </tr>`;
    let pagination = null;
    if (data && data.length > 0) {
      const headers =
        Array.isArray(this.header) && this.header.length > 0
          ? this.header
          : Object.keys(data[0] || []);

      const { page, rowsPerPage } = this;
      const rowsCount = data.length;

      const numberPages = Math.ceil(rowsCount / this.rowsPerPage);
      const sliceData = data.slice(
        page * rowsPerPage - rowsPerPage,
        page * rowsPerPage
      );

      thead = this.thead(headers, sort);

      tbody = this.tbody(sliceData);

      if (numberPages > 1)
        pagination = this.pagination(numberPages, rowsPerPage, page, rowsCount);
    }

    return html`
      <div class="sd-container">
        <div class="sd-wrapper">
          <table class="${fullWidth} ${bordered} ${striped} ${stickyHeader}">
            <thead>
              <tr>
                ${thead}
              </tr>
            </thead>
            <tbody>
              ${tbody}
            </tbody>
          </table>
        </div>
        ${pagination}
      </div>
    `;
  }
}
