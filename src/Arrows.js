import { html } from 'lit';
import { arrowPaginateColor, arrowHeadColor } from './css.js';

export const up = html` <svg
  class="pt-3"
  width="14"
  height="10"
  viewBox="0 0 14 12"
  fill="none"
  xmlns="http://www.w3.org/2000/svg"
>
  <path
    d="M1 7L6.29289 1.70711C6.68342 1.31658 7.31658 1.31658 7.70711 1.70711L13 7"
    stroke=${arrowHeadColor}
    stroke-width="1.5"
    stroke-linecap="round"
    stroke-linejoin="round"
  />
</svg>`;

export const down = html` <svg
  class="pt-3"
  width="14"
  height="10"
  viewBox="0 0 14 12"
  fill="none"
  xmlns="http://www.w3.org/2000/svg"
>
  <path
    d="M13 1L7.70711 6.29289C7.31658 6.68342 6.68342 6.68342 6.29289 6.29289L1 1"
    stroke=${arrowHeadColor}
    stroke-width="1.5"
    stroke-linecap="round"
    stroke-linejoin="round"
  />
</svg>`;

export const upDown = html` <svg
    width="14"
    height="10"
    viewBox="0 0 14 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M1 7L6.29289 1.70711C6.68342 1.31658 7.31658 1.31658 7.70711 1.70711L13 7"
      stroke=${arrowHeadColor}
      stroke-width="1.5"
      stroke-linecap="round"
      stroke-linejoin="round"
    />
  </svg>
  <svg
    width="14"
    height="10"
    viewBox="0 0 14 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M13 1L7.70711 6.29289C7.31658 6.68342 6.68342 6.68342 6.29289 6.29289L1 1"
      stroke=${arrowHeadColor}
      stroke-width="1.5"
      stroke-linecap="round"
      stroke-linejoin="round"
    />
  </svg>`;

export const first = html` <span>
  <svg
    width="7"
    height="11"
    viewBox="0 0 7 11"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M5.29375 10.5875L0 5.29375L5.29375 0L6.325 0.9625L1.99375 5.29375L6.325 9.625L5.29375 10.5875Z"
      fill="${arrowPaginateColor}"
    />
  </svg>
  <svg
    width="7"
    height="11"
    viewBox="0 0 7 11"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M5.29375 10.5875L0 5.29375L5.29375 0L6.325 0.9625L1.99375 5.29375L6.325 9.625L5.29375 10.5875Z"
      fill="${arrowPaginateColor}"
    />
  </svg>
</span>`;

export const last = html`<span>
  <svg
    width="7"
    height="11"
    viewBox="0 0 7 11"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M1.70624 10.5875L7 5.29375L1.70624 0L0.674988 0.9625L5.00623 5.29375L0.674988 9.625L1.70624 10.5875Z"
      fill="${arrowPaginateColor}"
    />
  </svg>
  <svg
    width="7"
    height="11"
    viewBox="0 0 7 11"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M1.70624 10.5875L7 5.29375L1.70624 0L0.674988 0.9625L5.00623 5.29375L0.674988 9.625L1.70624 10.5875Z"
      fill="${arrowPaginateColor}"
    />
  </svg>
</span>`;

export const back = html` <svg
  width="7"
  height="11"
  viewBox="0 0 7 11"
  fill="none"
  xmlns="http://www.w3.org/2000/svg"
>
  <path
    d="M5.29375 10.5875L0 5.29375L5.29375 0L6.325 0.9625L1.99375 5.29375L6.325 9.625L5.29375 10.5875Z"
    fill="${arrowPaginateColor}"
  />
</svg>`;

export const next = html` <svg
  width="7"
  height="11"
  viewBox="0 0 7 11"
  fill="none"
  xmlns="http://www.w3.org/2000/svg"
>
  <path
    d="M1.70624 10.5875L7 5.29375L1.70624 0L0.674988 0.9625L5.00623 5.29375L0.674988 9.625L1.70624 10.5875Z"
    fill="${arrowPaginateColor}"
  />
</svg>`;
