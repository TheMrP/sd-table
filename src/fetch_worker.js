const fetchWorkerCode = () => {
  // eslint-disable-next-line no-restricted-globals
  self.onmessage = event => {
    const voidReturn = [{error: 'The request fail or data can\'t be proceced'}];

    try {
      const options = event.data;

      // eslint-disable-next-line no-useless-escape
      const pattern = new RegExp(
        '(?:(?:https?)://|www.)(?:([-A-Z0-9+&@#/%=~_|$?!:,.]*)|[-A-Z0-9+&@#/%=~_|$?!:,.])*(?:([-A-Z0-9+&@#/%=~_|$?!:,.]*)|[A-Z0-9+&@#/%=~_|$])',
        'igm'
      );

      if (typeof options.uri !== 'string' || !pattern.test(options.uri)) {
        postMessage(voidReturn);
        throw new Error(`options.fetch.uri is not a valid URI ${options.uri}`);
      }

      fetch(options.uri, options.options)
        .then(async res => {
          if (res.status === 200) postMessage((await res.json()) || voidReturn);
          else
            postMessage(voidReturn);

        })
        .catch(() => {
          postMessage(voidReturn);
        });
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      postMessage(voidReturn);
    }
  };
};

let c = fetchWorkerCode.toString();
c = c.substring(c.indexOf('{') + 1, c.lastIndexOf('}'));

const blob = new Blob([c], { type: 'application/javascript' });
export const fetchWorker = URL.createObjectURL(blob);
