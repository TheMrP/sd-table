import { html, LitElement } from 'lit-element';
import { paginateStyle, arrowPaginateColor } from '../css.js';

export class SdTpaginate extends LitElement {
  static get styles() {
    return [paginateStyle];
  }

  static get properties() {
    return {
      loading: { type: Boolean },
      pagination: {
        type: Object,
        hasChanged(newVal, oldVal) {
          if (oldVal && oldVal.page !== undefined)
            return (
              parseInt(oldVal.page) !== parseInt(newVal.page) ||
              parseInt(oldVal.rowsPerPage) !== parseInt(newVal.rowsPerPage) ||
              parseInt(oldVal.numberPages) !== parseInt(newVal.numberPages)
            );
          else return false;
        },
      },
      changePage: { type: Object },
      rows: { type: Number },
    };
  }

  constructor() {
    super();

    this.page = 1;
    this.rows = 0;
    this.handleClick = this.handleClick.bind(this);
  }

  handleChange(e) {
    const rows = e.target.value || 15;
    this.handleClick(1);
    this.dispatchEvent(new CustomEvent('set-rows-per-page', { detail: rows }));
    this.requestUpdate().catch(err => console.log(err));
  }

  handleClick(page = 1) {
    this.page =
      page >= 1 && page <= this.pagination.numberPages ? page : this.page;
    this.dispatchEvent(new CustomEvent('set-page', { detail: this.page }));
    this.requestUpdate().catch(err => console.log(err));
  }

  render() {
    const pages = [];
    const pageItems = 4;
    const numberPages = this.pagination.numberPages || 1;
    const rowsPerPage = this.pagination.rowsPerPage || 15;
    const { page } = this;
    if (numberPages > 1) {
      const arrows = {
        first: html` <span>
          <svg
            width="7"
            height="11"
            viewBox="0 0 7 11"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M5.29375 10.5875L0 5.29375L5.29375 0L6.325 0.9625L1.99375 5.29375L6.325 9.625L5.29375 10.5875Z"
              fill="${arrowPaginateColor}"
            />
          </svg>
          <svg
            width="7"
            height="11"
            viewBox="0 0 7 11"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M5.29375 10.5875L0 5.29375L5.29375 0L6.325 0.9625L1.99375 5.29375L6.325 9.625L5.29375 10.5875Z"
              fill="${arrowPaginateColor}"
            />
          </svg>
        </span>`,
        back: html` <svg
          width="7"
          height="11"
          viewBox="0 0 7 11"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M5.29375 10.5875L0 5.29375L5.29375 0L6.325 0.9625L1.99375 5.29375L6.325 9.625L5.29375 10.5875Z"
            fill="${arrowPaginateColor}"
          />
        </svg>`,
        next: html` <svg
          width="7"
          height="11"
          viewBox="0 0 7 11"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M1.70624 10.5875L7 5.29375L1.70624 0L0.674988 0.9625L5.00623 5.29375L0.674988 9.625L1.70624 10.5875Z"
            fill="${arrowPaginateColor}"
          />
        </svg>`,
        last: html`<span>
          <svg
            width="7"
            height="11"
            viewBox="0 0 7 11"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M1.70624 10.5875L7 5.29375L1.70624 0L0.674988 0.9625L5.00623 5.29375L0.674988 9.625L1.70624 10.5875Z"
              fill="${arrowPaginateColor}"
            />
          </svg>
          <svg
            width="7"
            height="11"
            viewBox="0 0 7 11"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M1.70624 10.5875L7 5.29375L1.70624 0L0.674988 0.9625L5.00623 5.29375L0.674988 9.625L1.70624 10.5875Z"
              fill="${arrowPaginateColor}"
            />
          </svg>
        </span>`,
      };

      pages.push(
        html` <button
            id="sd-tpaginator-item-first"
            @click=${e => {
              e.preventDefault();
              this.handleClick(1);
            }}
          >
            ${arrows.first}
          </button>
          <button
            id="sd-tpaginator-item-back"
            @click=${e => {
              e.preventDefault();
              this.handleClick(page - 1);
            }}
          >
            ${arrows.back}
          </button>`,
      );

      for (let i = 1; i <= numberPages; i += 1) {
        const active = i === page ? 'active' : '';
        const minLimit = pageItems * 2 + 1;
        const show =
          (page <= pageItems && i <= minLimit) ||
          (i >= page - pageItems && i <= page + pageItems) ||
          (page >= numberPages - pageItems && i > numberPages - minLimit);

        pages.push(
          show
            ? html` <button
                id="sd-tpaginator-item-${i}"
                @click=${e => {
                  e.preventDefault();
                  this.handleClick(i);
                }}
                class=${active}
              >
                ${i}
              </button>`
            : html` <button
                id="sd-tpaginator-item-${i}"
                @click=${e => {
                  e.preventDefault();
                  this.handleClick(i);
                }}
                class=${active}
                ?hidden=${true}
              >
                ${i}
              </button>`,
        );
      }
      pages.push(
        html` <button
            id="sd-tpaginator-item-next"
            @click=${e => {
              e.preventDefault();
              this.handleClick(page + 1);
            }}
          >
            ${arrows.next}
          </button>
          <button
            id="sd-tpaginator-item-last"
            @click=${e => {
              e.preventDefault();
              this.handleClick(numberPages);
            }}
          >
            ${arrows.last}
          </button>`,
      );
    }

    const options = [];
    for (let i = 15; i <= 105; i += 15)
      options.push(
        rowsPerPage === i
          ? html` <option value=${i} selected>${i}</option>`
          : html` <option value=${i}>${i}</option>`,
      );

    options.push(html` <option value=${this.rows + 1}>All</option>`);

    const select = html`&nbsp;<select
        @change=${this.handleChange}
        aria-label="Filas por página"
      >
        ${options}</select
      >&nbsp;`;

    return html` <span class="left">
        Showing ${select} of ${this.rows} rows per page. Page ${page} of
        ${numberPages}
      </span>
      <div>${pages}</div>`;
  }
}
