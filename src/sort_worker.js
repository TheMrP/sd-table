const sortWorkerCode = () => {
  // eslint-disable-next-line no-restricted-globals
  self.onmessage = e => {
    const getValue = value => {
      const typeOf = typeof value;
      if (typeOf === 'string') {
        const cleanValue = value.trim().replace(',', '');
        const auxValue = cleanValue * 1 || 0;
        return auxValue !== 0 && cleanValue !== '0' ? auxValue : value;
      }
      if (typeOf === 'number') return value;
      if (typeOf === 'function') return value.value || value.toString();

      return '';
    };

    const { data, sort } = e.data;
    const sortKeys = Object.keys(sort);

    Promise.all(
      data.sort((a, b) => {
        let aCount = 0;
        let bCount = 0;

        for (let l = 0; l < sortKeys.length; l += 1) {
          const sortKey = sortKeys[l];
          const order = sort[sortKey];
          const sortDirection = order === 'asc' ? -1 : 1;

          const aValue = getValue(a[sortKey]);
          const bValue = getValue(b[sortKey]);

          if (typeof aValue === 'string' || typeof bValue === 'string') {
            const compare = aValue.toString().localeCompare(bValue.toString());
            if (compare > 0) aCount += sortDirection;

            if (compare < 0) bCount += sortDirection;
          } else {
            if (aValue > bValue) aCount += sortDirection;

            if (aValue < bValue) bCount += sortDirection;
          }
        }

        if (aCount > bCount) return 1;

        if (aCount < bCount) return -1;

        return 0;
      })
    ).then(orderedData => {
      postMessage(orderedData);
    });
  };
};

let c = sortWorkerCode.toString();
c = c.substring(c.indexOf('{') + 1, c.lastIndexOf('}'));

const blob = new Blob([c], { type: 'application/javascript' });

export const sortWorker = URL.createObjectURL(blob);
