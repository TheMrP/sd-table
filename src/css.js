import { css } from 'lit';

export const arrowHeadColor = css`var(--sd-thead-arrow-color, #000)`;
export const arrowPaginateColor = css`var(--sd-paginate-arrow-color, #000)`;

export const paginateStyle = css`
  .sd-pagination {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
    padding: 1rem 0 1rem 0;
  }

  .sd-pagination button,
  .sd-pagination a,
  .sd-pagination a:link,
  .sd-pagination a:visited {
    vertical-align: middle;
    padding: 0.3rem;
    border: none;
    border-bottom: solid 2px transparent;
    background: transparent;
    cursor: pointer;
    min-width: 2rem;
    min-height: 2rem;
    color: var(--sd-paginate-color, #000);
  }

  .sd-pagination button.active,
  .sd-pagination a.active,
  .sd-pagination button:hover,
  .sd-pagination a:hover {
    font-weight: bold;
    border-bottom: solid 2px var(--sd-paginate-item-border-color, #000);
  }

  .sd-pagination span {
    display: flex;
    align-items: center;
  }
`;

export const sdTableStyle = css`
  .sd-container {
    display: flex;
    flex-direction: column;
    padding: 1rem 0 1rem 0;
  }

  .sd-wrapper {
    display: flex;
    width: 100%;
    max-height: 570px;
    overflow: auto;
  }

  .sd-wrapper table {
    margin:0 auto;
    border-collapse: collapse;
  }

  .sd-wrapper .sticky-header thead th {
    position: sticky;
    top: 0;
  }

  .sd-wrapper thead th {
    text-align: var(--sd-thead-text-align, center);
    vertical-align: var(--sd-thead-vertical-align, middle);
    border: var(--sd-thead-border);
    border-color: var(--sd-thead-border-color);
    border-top: var(--sd-thead-border-top);
    border-right: var(--sd-thead-border-right);
    border-bottom: var(--sd-thead-border-bottom, solid 1px #000);
    border-left: var(--sd-thead-border-left);
    background: var(--sd-thead-background, #FFF);
    padding: var(--sd-thead-padding, 0.3rem);
    white-space: nowrap;
    color: var(--sd-thead-color, #000);
    font-size: var(--sd-thead-font-size, 1rem);
    font-style: var(--sd-thead-font-style);
    font-weight: var(--sd-thead-font-weight, bold);
  }

  .sd-wrapper thead button,
  .sd-wrapper thead a,
  .sd-wrapper thead a:link,
  .sd-wrapper thead a:visited {
    text-align: var(--sd-thead-text-align, center);
    vertical-align: var(--sd-thead-vertical-align, middle);
    color: var(--sd-thead-color, #000);
    font-size: var(--sd-thead-font-size, 1rem);
    font-style: var(--sd-thead-font-style);
    font-weight: var(--sd-thead-font-weight, bold);
    background: transparent;
    border: none;
    cursor: pointer;
    display: flex;
    height: 20px;
    width: 100%;
    justify-content: center;
  }

  .sd-wrapper thead span {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: var(--sd-thead-justify-content, center);
    padding-right: 0.5rem;
  }

  .sd-wrapper td {
    padding: var(--sd-tbody-padding, 0.3rem);
    font-size: var(--sd-tbody-font-size, 1rem);
    text-align: var(--sd-tbody-text-align, justify);
    color: var(--sd-tbody-color, #5c5c5c);
    vertical-align: var(--sd-tbody-vertical-align, middle);
  }

  .sd-wrapper .full-width {
    width: 99%;
  }

  .sd-wrapper .striped tbody tr:nth-child(even) {
    background: var(--sd-striped-even-color, #ffffff);
  }

  .sd-wrapper .striped tbody tr:nth-child(odd) {
    background: var(--sd-striped-odd-color, #efefef);
  }

  .sd-wrapper .bordered td,
  .sd-wrapper .bordered th {
    border: var(--sd-bordered-border, solid 1px #ccc);
  }
`;
